#!/usr/bin/env python3

import socket
import nmap
from tqdm import tqdm

def get_local_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    local_ip = s.getsockname()[0]
    s.close()
    return(local_ip[:10])

def scan_network():
    subnet = get_local_ip() + "1/24"
    nm = nmap.PortScanner()
    nm.scan(hosts=subnet, arguments='-sn')
    hosts_list = [(x, nm[x].hostname(), nm[x]['status']['state']) for x in nm.all_hosts()]
    print("\n====== NETWORKED HOSTS ======\n")
    for host, hostname, status in tqdm(hosts_list):
        print("{0}\t[{1}] is {2}".format(host, hostname, status))
    print()

if __name__ == '__main__':
   scan_network()
